/**
* 收藏 列表 详情页
*/
'use strict'

var HTMLWebView = require('react-native-html-webview');

import Api from "../common/Api"
import Util from "../common/Util"

var moment=require('moment')
var zhCn=require('moment/locale/zh-cn')

var React = require('react-native');
var {
  ScrollView,
  StyleSheet,
  View,
  Text,
  Dimensions
}=React;


export default class Detail extends React.Component{
  constructor(props){
    super(props)
    this.state={
      blog:{},
      show:false
    }
  }

  componentDidMount(){
    const that=this;
    const {objid}=this.props;
    console.log('objid='+objid);
    var url =[Api.blog_detail,objid].join('/');
    Util.get(url,(data)=>{
      console.log('blog_detail:'+JSON.stringify(data));
      that.setState({
        blog:data.oschina.blog,
        show:true,
      })
    })

  }

  render(){
    const data=this.state.blog;
    const pubDate=moment(data.pubDate,'YYYY-MM-DD hh:mm:ss').startOf('hour').fromNow();
    return (
      <View style={styles.container}>
      {
        this.state.show && data.title!==undefined?
        <ScrollView >
          <View>
            <Text style={{fontWeight:'bold',fontSize:17}}>{data.title}</Text>
          </View>
          <View style={{flexDirection:'row',marginTop:5,marginBottom:10}}>
            <Text style={{color:'green',fontWeight:'bold',fontSize:13}}>{data.author}</Text>
            <Text style={{color:'gray',fontSize:13,marginLeft:5}}>{pubDate}</Text>
          </View>
          <HTMLWebView
            style={{flex:1}}
            html={data.body}
            makeSafe={true}
            autoHeight={true}
            />
        </ScrollView>
        : Util.loading
      }
      </View>
    );
  }
}

var styles=StyleSheet.create({
  container:{
    //backgroundColor:'#eeedf1',
    flex:1,
    marginTop:5,
    marginLeft:5,
    marginRight:5,
    flexDirection:'column',
  }
});
