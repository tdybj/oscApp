'use strict'


import Util from "../common/Util"
import Api from "../common/Api"

import MeApp from "./index"

var React = require('react-native');
var AdSupportIOS = require('AdSupportIOS');

var {
  View,
  Text,
  StyleSheet,
  TouchableHighlight,
  TextInput,
  AsyncStorage,
  AlertIOS,
}=React;


export default class Login extends React.Component{
  constructor(props){
    super(props)
    this.state={
      username:'',
      pwd:''
    }
  }

  _login(){
    var username=this.state.username;
    var pwd =this.state.pwd;



    // AdSupportIOS.getAdvertisingTrackingEnabled(function(){
    //   AdSupportIOS.getAdvertisingId(function(deviceId){
    //
    //   },function(){
    //     AlertIOS.alert('设置','无法获取设备唯一标识');
    //   })
    // }, function(){
    //   AlertIOS.alert('设置','无法获取设备唯一标识，请关闭设置->隐私->广告->限制广告跟踪');
    // });
    var that =this;
    Util.post(Api.login,{
      username:username,
      pwd:pwd
    },function(data){
      if (data.data.oschina.result.errorCode==="1") {
        var user=data.data.oschina.user;
        var notice=data.data.oschina.notice;
        var key =data.key;
        console.log('data='+JSON.stringify(data));
        console.log('key='+key);
        AsyncStorage.multiSet([
          ['uid', user.uid],
          ['location', user.location],
          ['name', user.name],
          ['key', key],
        ], function(err){
          if(!err){
          //  AlertIOS.alert('登录',user.name+'登录成功');
            that.props.navigator.push({
              component:<MeApp/>,
              title: "我"
            })
          }
        });
      } else {
        AlertIOS.alert('登录', '用户名或者密码错误');
      }

    },function(err){
      AlertIOS.alert('登录',err);
    });

  }

  render(){
    return (
      <View style={styles.container}>

        <View style={styles.inputRow}>
          <Text>邮箱</Text>
          <TextInput style={styles.input} placeholder="请输入邮箱" onChangeText={(text)=>this.setState({username:text})}/>
        </View>
        <View style={styles.inputRow}>
          <Text>密码</Text>
          <TextInput style={styles.input} placeholder="请输入密码" password={true} onChangeText={(text)=>this.setState({pwd:text})}/>
        </View>

        <View>
          <TouchableHighlight underlayColor="#fff" style={styles.btn} onPress={this._login.bind(this)}>
            <Text style={{color:'#fff'}}>登录</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container:{
    marginTop:50,
    alignItems:'center',
  },
  inputRow:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent: 'center',
    marginBottom:10,
  },
  input:{
    marginLeft:10,
    width:220,
    borderWidth:Util.pixel,
    height:35,
    paddingLeft:8,
    borderRadius:5,
    borderColor:'#ccc'
  },
  btn:{
    marginTop:10,
    width:180,
    height:35,
    backgroundColor:'#d97228',
    justifyContent:'center',
    alignItems:'center',
    borderRadius: 4,
  }
});
