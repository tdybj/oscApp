
import Util from "../common/Util"

var moment=require('moment')
var zhCn=require('moment/locale/zh-cn')

var React=require('react-native')
var {
  View,
  Text,
  StyleSheet,
  TouchableOpacity
}=React;

class NewsItem extends React.Component {
  constructor(props){
    super(props)

  }

  render(){
    var {row}=this.props;
    const pubDate=moment(row.pubDate,'YYYY-MM-DD hh:mm:ss').startOf('hour').fromNow();

    return (
      <TouchableOpacity style={[styles.row, styles.item]} {...this.props}>
          <View style={styles.itemHeader}>
            <Text style={styles.headerText}>{row.title}</Text>
          </View>
          <View style={styles.itemBody}>
            <Text style={styles.bodyText}>{row.body}</Text>
          </View>
          <View style={styles.itemFooter}>
            <View style={styles.footerContent}>
              <Text style={styles.authorText}>{row.author===undefined?row.authorname:row.author}</Text>
              <Text style={styles.dateText}>{pubDate}</Text>
            </View>
          </View>
    </TouchableOpacity>
    );
  }

}

var styles=StyleSheet.create({
  row:{
    flexDirection:'column',

  },
  item:{
    minHeight:80,
    borderBottomWidth:Util.pixel,
    borderColor:'#ccc',
    paddingBottom:3,
    paddingTop:5,
  },
  itemContent:{
    flex:1,
  },
  itemHeader:{

  },
  headerText:{
    fontWeight:'bold',
    fontSize:15,
  },
  itemBody:{

  },
  bodyText:{
    fontWeight:"100",
    color:"gray",
    fontSize:13
  },
  itemFooter:{
    marginTop:5,
  },
  footerContent:{
    flexDirection:'row',

  },
  authorText:{
    color:'#4ead7d',
    paddingRight:5,
    fontSize:12,
  },
  dateText:{
    color:"gray",
    fontSize:12,
  }
});

export default NewsItem;
