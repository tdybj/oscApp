/**
  @description ScrollView  http://reactnative.cn/docs/scrollview.html#content
  props:
    1、horizontal:bool
      当此属性为true的时候，所有的的子视图会在水平方向上排成一行，而不是默认的在垂直方向上排成一列。默认值为false。
    2、
*/

'use strict'

var React = require('react-native');
var {
  View,
  ScrollView,
  Text,
  StyleSheet,
  PixelRatio,
} =  React;

export default class ScrollViewDemo extends React.Component{
  constructor(props){
    super(props)
    this.state={
      list:['row0','row1','row2','row3','row4','row5','row6','row7','row8','row9'],
    }
  }

  _renderRow(row,i){
    return (
      <View key={i} style={styles.item}>
        <Text>{row}</Text>
      </View>
    )
  }

  render(){
    return (
      <ScrollView
        >
        {this.state.list.map((row,i)=>this._renderRow(row,i))}
      </ScrollView>
    );
  }
}

var styles = StyleSheet.create({
item:{
  flexDirection:'column',
  borderBottomWidth:2 / PixelRatio.get(),
  borderColor:'gray',
  minHeight:80,
  alignItems:'center'

},
});
