/**
  @description ListView
  Demo https://facebook.github.io/react-native/docs/listview.html#content
  props说明：
    1、initialListSize: number
      首次初始化渲染的条数，一般大于首屏条数；
    2、pageSize: Number
      每次事件循环渲染的条数。
    3、onChangeVisibleRows: func
      当可见的行集发生改变时调用，有两个参数：(visibleRows, changedRows)，changedRows为可见性发生改变的行集；visibleRows为可见行集。
      如：initialListSize={1}时，每渲染一条数据就会调用一次onChangeVisibleRows函数.
    4、onEndReachedThreshold: number
      调用 onEndReached 方法的阀值。
    5、onEndReached: func
      当所有行已经呈现并且列表被滚动到了距离底部只有onEndReachedThreshold个像素时被调用.
    6、renderFooter 和 renderHeader : func
      用于渲染页脚和页眉。每次呈现都会调用（如：在页面A中配置了renderFooter,则每次从其它页面转到页面A时，每次从页面A转到其它页面时 都会调用renderFooter函数。）
      所以如果重新呈现它们耗费很大，那就把它们包在 StaticContainer 或其他适当的机制中。
    7、renderRow: func
      渲染每条数据。函数定义为：(rowData, sectionID, rowID) => renderable。
    8、renderScrollComponent: func ？？没理解使用场景
      返回的组件用来渲染list rows
      默认函数定义为：props => <ScrollView {...props} />.和renderFooter一样每次呈现都会调用。
    9、renderSectionHeader: func ??
      函数定义：(sectionData, sectionID) => renderable
      根据官方文档理解应该是：如果提供了该函数，会在该section上渲染一个HEAD，且屏幕滚动时将一直sticky(粘)在屏幕上，直到下一个section。
      但测试没有发现该功能，不知道是什么原因，待研究！！
    10、renderSeparator: func
      函数定义：(sectionID, rowID, adjacentRowHighlighted) => renderable
      用于渲染在每行之间的信息
    11、scrollRenderAheadDistance: Number
      顶部屏幕之外的数据离屏幕头部多远时（像素）开始渲染数据。默认值为：1000
    12、horizontal: bool ??没理解使用场景
    13、stickyHeaderIndices: [number] （
      配置滚动时哪些行应该停驻在屏幕顶部。如：stickyHeaderIndices={[0]}，第一行数据将停驻在屏幕顶部。
      这个属性不能和horizontal一起使用。
      该props在ListView中无效，在ScrollView中才有效。ListView要实现sticky效果可以参考：http://www.ghugo.com/react-native-listview-stickyheaderindices/
    14、onResponderGrant ,onScroll ,onResponderRelease : func
      (event)=>operate
      屏幕滚动时调用。调用顺序：onResponderGrant ,onScroll ,onResponderRelease。
      onResponderGrant：触摸的起点触发。
      onScroll：屏幕滚动时被反复触发。
      onResponderRelease：在触摸最后被触发，即手指离开屏幕时。
      注意：这三个函数不能和 renderScrollComponent 一起使用。如果一起使用，这三个函数将不会执行。
    15、onResponderMove: func
      (event)=>{}  用户移动手指时触发。
      与onScroll的区别：onResponderMove，当用户手指离开屏幕后不再触发；而onScroll，用户手指离开屏幕后会继续触发，直到屏幕停止滚动。

  其它props：
    removeClippedSubviews: bool
    对大型列表滚动性能做优化，默认为 true。实验性质的属性，提高滚动时到性能，和row的样式‘overflow:hidden’一起使用


*/
'use strict'

var React = require('react-native');
var {
  View,
  ListView,
  Text,
  StyleSheet,
  PixelRatio,
  TouchableOpacity,
  ScrollView,
}=React;

var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
export default class ListViewDemo extends React.Component{
    constructor(props){
      super(props)
      this.state={
        list:['row0','row1','row2','row3','row4','row5','row6','row7','row8','row9'],
        datasource:ds.cloneWithRows([]),
      }
    }

    /**
      @description 渲染行集中的每条数据
      @param rowData
      @param sectionID ？？没理解
      @param rowID 行集中数据编号，从0开始。
      @param highlightRow 函数。??怎么使用
    */
    _renderRow(rowData, sectionID, rowID, highlightRow){
      //exec renderRow .....rowData:row0,sectionID:s1,rowID:0,highlightRow:function () { [native code] }
      //exec renderRow .....rowData:row1,sectionID:s1,rowID:1,highlightRow:function () { [native code] }
    //  console.log('exec renderRow .....rowData:'+rowData+',sectionID:'+sectionID+',rowID:'+rowID+',highlightRow:'+highlightRow);
      return (
        <View style={styles.item}>
          <Text>{rowData}</Text>
        </View>
      )
    }

    _onEndReached(){
      console.log('exec onEndReached......')
    }

    _loadMore(){
      var tmp = Array.from([0, 1, 2], (x) =>'row'+(this.state.list.length+x));
      this.setState({
        list:this.state.list.concat(tmp),
      })
    }

    _onRefresh(){

    }

    _renderFooter(){
      console.log('exec renderFooter......');
      return (
        <TouchableOpacity onPress={this._loadMore.bind(this)}>
          <View style={styles.footer}>
            <Text>点击加载更多</Text>
          </View>
        </TouchableOpacity>

      );
    }

    _renderHeader(){
      console.log('exec renderHeader.....')
      return (
        <TouchableOpacity onPress={this._onRefresh.bind(this)}>
          <View style={styles.header}>
            <Text>点击刷新</Text>
          </View>
        </TouchableOpacity>
      );
    }

    _renderScrollComponent(){
      console.log('exec renderScrollComponent.......')
      return (
        <ScrollView style={{flex:1}}>
          <Text>renderScrollComponent</Text>
        </ScrollView>
      )
    }

    _renderSectionHeader(sectionData, sectionID){
      console.log('exec renderSectionHeader.....sectionData:'+sectionData+', sectionID:'+sectionID)
      return (
        <View>
          <Text>renderSectionHeader</Text>
        </View>
      )
    }

    _renderSeparator(sectionID, rowID, adjacentRowHighlighted){
      console.log('exec renderSeparator .....sectionID:'+sectionID+',rowID:'+rowID+',adjacentRowHighlighted:'+adjacentRowHighlighted);
      return (
        <View key={rowID}>
          <Text>renderSeparator</Text>
        </View>
      )
    }

    _onResponderGrant(event){
      var nativeEvent = event.nativeEvent;
      var contentInset=nativeEvent.contentInset;
      var contentOffset=nativeEvent.contentOffset;
      var contentSize=nativeEvent.contentSize;
      var layoutMeasurement =nativeEvent.layoutMeasurement;
      if(!contentInset){
        console.log('exec onResponderGrant contentInset is null');
        console.log(`exec onResponderGrant.....locationX:${nativeEvent.locationX};locationY:${nativeEvent.locationY};pageX:${nativeEvent.pageX};pageY:${nativeEvent.pageY};target:${nativeEvent.target}`);
        return;
      }
      console.log(`exec onResponderGrant.....;contentInset:[bottom,left,right,top]:[${contentInset.bottom},${contentInset.left},${contentInset.right},${contentInset.top}];contentOffset:[x,y]:[${contentOffset.x},${contentOffset.y}];contentSize:[height:width]:[${contentSize.height},${contentSize.width}];layoutMeasurement:[height:width]:[${layoutMeasurement.height},${layoutMeasurement.width}]`);



    }

    _onResponderRelease(event){
      var nativeEvent = event.nativeEvent;
      // identifier     ——触摸的 ID
      // locationX: 164 --触摸相对于元素的 X 位置
      // locationY: -9  --触摸相对于元素的 Y 位置
      // pageX: 164     --触摸相对于屏幕的 X 位置
      // pageY: 254     --触摸相对于屏幕的 Y 位置
      // target: 363    接收触摸事件的元素的节点 id
      // timestamp: 31407320.392311 --触摸的时间标识符，用于速度计算
      //touches         ——所有当前在屏幕上触摸的数组
      console.log(`exec onResponderRelease.....locationX:${nativeEvent.locationX};locationY:${nativeEvent.locationY};pageX:${nativeEvent.pageX};pageY:${nativeEvent.pageY};target:${nativeEvent.target}`);

    }

    _onScroll(event){
      var nativeEvent = event.nativeEvent;
      var contentInset=nativeEvent.contentInset;
      var contentOffset=nativeEvent.contentOffset;
      var contentSize=nativeEvent.contentSize;
      var layoutMeasurement =nativeEvent.layoutMeasurement;
      console.log(`exec onScroll.....;contentInset:[bottom,left,right,top]:[${contentInset.bottom},${contentInset.left},${contentInset.right},${contentInset.top}];contentOffset:[x,y]:[${contentOffset.x},${contentOffset.y}];contentSize:[height:width]:[${contentSize.height},${contentSize.width}];layoutMeasurement:[height:width]:[${layoutMeasurement.height},${layoutMeasurement.width}]`);


    }

    _onResponderMove(event){
      console.log('exec onResponderMove ......')
    }

    render(){
      return (
        <ListView
          dataSource={ds.cloneWithRows(this.state.list)}
          renderRow={(rowData, sectionID, rowID, highlightRow)=>this._renderRow(rowData, sectionID, rowID, highlightRow)}

          initialListSize={9}
          pageSize={3}

          onChangeVisibleRows={(visibleRows, changedRows) => console.log({visibleRows, changedRows})}
          onEndReachedThreshold={50}
          onEndReached={this._onEndReached}

          //renderFooter={this._renderFooter.bind(this)}
          //renderHeader={this._renderHeader.bind(this)}

          //renderScrollComponent={this._renderScrollComponent}
          renderSectionHeader={(sectionData, sectionID)=>this._renderSectionHeader(sectionData, sectionID)}
          renderSeparator={(sectionID, rowID, adjacentRowHighlighted)=>this._renderSeparator(sectionID, rowID, adjacentRowHighlighted)}

          scrollRenderAheadDistance={100}

          stickyHeaderIndices={[0]}

          //horizontal={true}

          onResponderGrant={this._onResponderGrant}
          onResponderRelease={this._onResponderRelease}
          onScroll={this._onScroll}
          onResponderMove={this._onResponderMove}
        />
      )
    }
}

var styles =StyleSheet.create({
  item:{
    flexDirection:'column',
    borderBottomWidth:2 / PixelRatio.get(),
    borderColor:'gray',
    minHeight:80,
    alignItems:'center'

  },
  footer:{
    marginTop:15,
    marginBottom:10,
    justifyContent:'center',
    alignItems:'center'
  },
  header:{
      marginTop:10,
      marginBottom:15,
      justifyContent:'center',
      alignItems:'center'
  }
})
