/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

import NavigatorWrapper from "./ios_views/common/NavigatorWrapper"
import News from "./ios_views/news/news_list"

var React = require('react-native');
var {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TabBarIOS,
  StatusBarIOS
} = React;

//StatusBarIOS.setStyle({backgroundColor:'#4ead7d'});
module.exports = React.createClass({
  getInitialState:function(){
    return {
      selectedTab:"综合"
    }
  },
  _renderContent:function(contentTab,color){
    return (
      <View style={styles.contentTab}>
        <Text style={[styles.contentText,{color:color}]}>{contentTab}</Text>
      </View>
    )
  },
  render: function() {
    return (
      <TabBarIOS
        style={{elevation:1000}}
        translucent={false}>
          <TabBarIOS.Item
              title="综合"
              icon={this.state.selectedTab==="综合"?require('image!tabbar-news-selected'):require('image!tabbar-news')}
              selected={this.state.selectedTab === "综合"}
              onPress={()=>{
                  this.setState({
                      selectedTab:"综合"
                  })
              }}>
              <News navigator={this.props.navigator} route={this.props.route} {...this.props.route.passProps}/>
          </TabBarIOS.Item>

          <TabBarIOS.Item
              title="动弹"
              icon={this.state.selectedTab==="动弹"?require('image!tabbar-tweet-selected'):require('image!tabbar-tweet')}
              selected={this.state.selectedTab === "动弹"}
              onPress={()=>{
                  this.setState({
                      selectedTab:"动弹"
                  })
              }}>
              {this._renderContent("动弹","#eb3219")}
          </TabBarIOS.Item>

          <TabBarIOS.Item
              title="发现"
              icon={this.state.selectedTab==="发现"?require('image!tabbar-discover-selected'):require('image!tabbar-discover')}
              selected={this.state.selectedTab === "发现"}
              onPress={()=>{
                  this.setState({
                      selectedTab:"发现"
                  })
              }}>
              {this._renderContent("发现","#eb3219")}
          </TabBarIOS.Item>

          <TabBarIOS.Item
              title="我的"
              icon={this.state.selectedTab==="我的"?require('image!tabbar-me-selected'):require('image!tabbar-me')}
              selected={this.state.selectedTab === "我的"}
              onPress={()=>{
                  this.setState({
                      selectedTab:"我的"
                  })
              }}>
              {this._renderContent("我的","#eb3219")}
          </TabBarIOS.Item>

      </TabBarIOS>
    );
  }
});

var styles = StyleSheet.create({
  contentTab:{
    flex:1,
    alignItems:'center',//水平居中
    justifyContent:'center'//垂直居中

  },
  contentText:{

  },
});

//AppRegistry.registerComponent('oscApp', () => oscApp);
